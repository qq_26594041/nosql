package com.du.dao;

import com.du.bean.Spit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author 杜瑞龙
 * &#064;date  2023/10/9 23:36
 */
@Repository
public interface SpitDao extends MongoRepository<Spit, String> {


    public Page<Spit> findByParentid(String parentid, Pageable pageable);
}