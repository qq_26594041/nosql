package com.du.original;

import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Mongo数据库连接管理类
 *
 * @author 杜瑞龙
 * &#064;date  2023/10/5 21:59
 */
public class MongoManager {

    private static MongoClient mongoClient = null;

    private static void init() {
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyToClusterSettings(builder -> builder.hosts(List.of(new ServerAddress(Config.getHost(), Config.getPort()))))
                .applyToConnectionPoolSettings(builder -> builder.minSize(2).maxConnecting(2).maxWaitTime(10, TimeUnit.MINUTES))
                .build();

        mongoClient = MongoClients.create(settings);

    }

    public static MongoDatabase getDatabase() {
        if (mongoClient == null) {
            init();
        }
        return mongoClient.getDatabase("testDemo");
    }
}