package com.du.original;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author 杜瑞龙
 * &#064;date  2023/10/5 21:54
 */
@Slf4j
public class Config {

    static {
        try {
            Properties p = new Properties();
            InputStream input = Config.class.getClassLoader().getResourceAsStream("mongo.properties");
            p.load(input);
            host = p.getProperty("host");
            port = Integer.parseInt(p.getProperty("port"));
            if (input != null) {
                input.close();
            }
        } catch (IOException e) {
            log.error("加载异常", e);
        }
    }

    @Getter
    private static String host;
    @Getter
    private static int port;

}