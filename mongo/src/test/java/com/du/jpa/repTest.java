package com.du.jpa;

import com.du.bean.Spit;
import com.du.dao.SpitDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 杜瑞龙
 * &#064;date  2023/10/9 23:39
 */
@SpringBootTest
public class repTest {

    @Autowired
    SpitDao spitDao;

    @Test
    public void insert() {
        List<Spit> spits = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Spit spit = new Spit();
            spit.setContent("ee" + i);
            spit.setComment(1);
            spit.setPublishtime(new Date());
            spit.setNickname("d" + i);
            spit.setUserid("ooo" + i);
            spit.setVisits(1 + i);
            spit.setParentid("20");

            spits.add(spit);
        }
        spitDao.insert(spits);
    }

    @Test
    public void update() {

        Spit spit = new Spit();
        spit.set_id("65242337c564304db1935959");
        spit.setContent("ee");

    }

    @Test
    public void delete() {
        Spit spit = new Spit();
        spit.set_id("65242337c564304db1935957");
        spit.setContent("ee");
        spitDao.delete(spit);
        spitDao.deleteAll();
    }

    @Test
    public void find() {
        List<Spit> all = spitDao.findAll();
    }

    @Test
    public void findPage() {
        int page = 3;
        PageRequest pageRequest = PageRequest.of(page - 1, 10);
        Page<Spit> byParentid = spitDao.findByParentid("20", pageRequest);
        System.out.println();
    }

}
