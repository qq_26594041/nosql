package com.du.jpa;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
 * @author 杜瑞龙
 * @date 2023/10/18 0:03
 */
@SpringBootTest
public class QueryTest {
    @Autowired
    MongoTemplate mongoTemplate;

    @Test
    public void test() {
        Query query = new Query();
        query.addCriteria(Criteria.where("parentid").is("20"));
        Update update = new Update();
        update.inc("comment",2);
        mongoTemplate.updateMulti(query, update, "spit");

    }
}
