package com.du.original;

import com.du.original.bean.Address;
import com.du.original.bean.Person;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Filters.eq;

/**
 * @author 杜瑞龙
 * @date 2023/10/6 12:18
 */
public class InsertTest {

    @Test
    public void insert() {
        MongoDatabase database = MongoManager.getDatabase();
        MongoCollection<Document> collection = database.getCollection("student");
        Map<String, Object> map = new HashMap<>();
        map.put("name", "红孩儿");
        map.put("sex", "女");
        map.put("age", 35.0);
        map.put("address", "芭蕉洞");
        Document doc = new Document(map);
        collection.insertOne(doc);
        collection.find(eq("name","红孩儿")).forEach(document -> System.out.println(document.toJson()));
    }

    @Test
    public void insertObject() {
        //定义对象的解码注册器
        CodecRegistry pojoCodecRegistry = CodecRegistries.
                fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                        CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        // 选择数据库 并且 注册解码器
        MongoDatabase mongoDatabase = MongoManager.getDatabase().withCodecRegistry(pojoCodecRegistry);
        MongoCollection<Person> person = mongoDatabase
                .getCollection("person", Person.class);
        Person persons = new Person(ObjectId.get(), "张三", 20,new Address("人民路", "上海市", "666666"));
        person.insertOne(persons);

        List<Person> personList = Arrays.asList(new Person(ObjectId.get(), "张三",
                        20, new Address("人民路", "上海市", "666666")),
                new Person(ObjectId.get(), "李四", 21, new Address("北京西路", "上海市", "666666")),
        new Person(ObjectId.get(), "王五", 22, new Address("南京东路", "上海市", "666666")),
        new Person(ObjectId.get(), "赵六", 23, new Address("陕西南路", "上海市", "666666")),
        new Person(ObjectId.get(), "孙七", 24, new Address("南京西路", "上海市", "666666")));

        person.insertMany(personList);

    }

    @Test
    public void delete() {
        MongoDatabase database = MongoManager.getDatabase();
        MongoCollection<Document> collection = database.getCollection("student");
        Map<String, Object> map = new HashMap<>();
        map.put("name", "铁扇公主");
        Document doc = new Document(map);
        collection.deleteOne(doc);
    }

    @Test
    public void update() {
        MongoDatabase database = MongoManager.getDatabase();
        MongoCollection<Document> collection = database.getCollection("student");
//修改的条件
        BasicDBObject bson = new BasicDBObject("name", "红孩儿");
//修改后的值
        BasicDBObject bson2 = new BasicDBObject("$set", new BasicDBObject("address", "南海"));
//参数1：修改条件  参数2：修改后的值
        collection.updateOne(bson, bson2);
    }

    @Test
    public void update2() {
        MongoDatabase database = MongoManager.getDatabase();
        MongoCollection<Document> collection = database.getCollection("student");
        collection.updateOne(eq("name", "红孩儿"), Updates.set("address", "南海"));
    }
}
