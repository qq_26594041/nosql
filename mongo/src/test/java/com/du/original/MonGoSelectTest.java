package com.du.original;

import com.du.original.bean.Person;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import static com.mongodb.client.model.Filters.*;

/**
 * @author 杜瑞龙
 * &#064;date  2023/10/5 22:47
 */

public class MonGoSelectTest {

    @Test
    public void findAll() {
        MongoDatabase database = MongoManager.getDatabase();
        MongoCollection<Document> col = database.getCollection("col");
        FindIterable<Document> documents = col.find();
        for (Document document : documents) {
            System.out.println(document.get("likes"));
        }

        col.find(
                        and(lte("age", 50), gte("id", 100))
                )
                .sort(Sorts.descending("id"))
                .projection(
                        Projections.fields(
                                Projections.include("id", "age"),
                                Projections.excludeId()
                        )
                )
                .forEach((Consumer<? super Document>) document -> System.out.println(document.toJson()));
    }

    @Test
    public void findAll12() {
        MongoDatabase database = MongoManager.getDatabase();
        MongoCollection<Document> col = database.getCollection("user");
        col.find(
                        and(lte("age", 50), gte("id", 100))
                )
                .sort(Sorts.descending("id"))
                .projection(
                        Projections.fields(
                                Projections.include("id", "age"),
                                Projections.excludeId()
                        )
                )
                .forEach((Consumer<? super Document>) document -> System.out.println(document.toJson()));
    }


    /**
     * 匹配查询
     */
    @Test
    public void findAllByCondition() {
        MongoDatabase database = MongoManager.getDatabase();
        MongoCollection<Document> col = database.getCollection("col");
        BasicDBObject likes = new BasicDBObject("likes", 100);
        FindIterable<Document> documents = col.find(likes);
        for (Document document : documents) {
            System.out.println(document.get("likes"));
        }
    }

    /**
     * 模糊查询
     * （1）完全匹配Pattern pattern = Pattern.compile("^name$");
     * （2）右匹配Pattern pattern = Pattern.compile("^.*name$");
     * （3）左匹配Pattern pattern = Pattern.compile("^name.*$");
     * （4）模糊匹配Pattern pattern = Pattern.compile("^.*name.*$");
     */
    @Test
    public void findAllLike() {

        MongoDatabase database = MongoManager.getDatabase();
        Pattern queryPattern = Pattern.compile("^.*菜.*$");
        BasicDBObject bson = new BasicDBObject("by", queryPattern);
        MongoCollection<Document> col = database.getCollection("col");
        FindIterable<Document> documents = col.find(bson);
        for (Document document : documents) {
            System.out.println(document.get("likes"));
        }
    }

    @Test
    public void findAllLt() {
        MongoDatabase database = MongoManager.getDatabase();
        BasicDBObject bson = new BasicDBObject("likes", new BasicDBObject("$lt", 200));
        MongoCollection<Document> col = database.getCollection("col");
        FindIterable<Document> documents = col.find(bson);
        for (Document document : documents) {
            System.out.println(document.get("likes"));
        }
    }

    @Test
    public void findAllAnd() {
        MongoDatabase database = MongoManager.getDatabase();
        BasicDBObject bson1 = new BasicDBObject("age", new BasicDBObject("$gte", 20));
        //查询年龄小于30的
        BasicDBObject bson2 = new BasicDBObject("age", new BasicDBObject("$lt", 30));
        //构建查询条件and
        BasicDBObject bson = new BasicDBObject("$and", Arrays.asList(bson1, bson2));
        MongoCollection<Document> col = database.getCollection("col");
        FindIterable<Document> documents = col.find(bson);
        for (Document document : documents) {
            System.out.println(document.get("likes"));
        }
    }

    @Test
    public void findAllOr() {
        MongoDatabase database = MongoManager.getDatabase();
        BasicDBObject bson1 = new BasicDBObject("age", new BasicDBObject("$lte", 20));
        BasicDBObject bson2 = new BasicDBObject("sex", "女");
//构建查询条件or
        BasicDBObject bson = new BasicDBObject("$or", Arrays.asList(bson1, bson2));
        MongoCollection<Document> col = database.getCollection("col");
        FindIterable<Document> documents = col.find(bson);
        for (Document document : documents) {
            System.out.println(document.get("likes"));
        }
    }


    /**
     * 分页
     *
     */
    public static Map<String, Object> listPage(String logName, Map<String, Object> map, int pageIndex, int pageSize) {
        MongoDatabase database = MongoManager.getDatabase();
        MongoCollection<Document> collection = database.getCollection(logName);
        BasicDBObject bson = new BasicDBObject(map);
        FindIterable<Document> find = collection.find(bson);
        int skip = (pageIndex - 1) * pageSize;
        find.skip(skip);//跳过记录数
        find.limit(pageSize);//一页查询记录数
        long count = collection.countDocuments(bson);
        Map<String, Object> m = new HashMap<>();
        m.put("total", count);
        m.put("rows", find);
        return m;
    }


    @Test
    public void selectObject() {
        //定义对象的解码注册器
        CodecRegistry pojoCodecRegistry = CodecRegistries.
                fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                        CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        // 选择数据库 并且 注册解码器
        MongoDatabase mongoDatabase = MongoManager.getDatabase().withCodecRegistry(pojoCodecRegistry);

        MongoCollection<Person> personCollection = mongoDatabase
                .getCollection("person", Person.class);
        personCollection.find(Filters.eq("name", "张三"))
                .forEach((Consumer<? super Person>) System.out::println);
    }

    @Test
    public void testUpdate() {
        //定义对象的解码注册器
        CodecRegistry pojoCodecRegistry = CodecRegistries.
                fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                        CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        // 选择数据库 并且 注册解码器
        MongoDatabase mongoDatabase = MongoManager.getDatabase().withCodecRegistry(pojoCodecRegistry);

        MongoCollection<Person> personCollection = mongoDatabase
                .getCollection("person", Person.class);
        UpdateResult updateResult =
                personCollection.updateMany(Filters.eq("name", "张三"), Updates.set("age",
                        22));

        System.out.println(updateResult);
    }

    @Test
    public void delete() {
        //定义对象的解码注册器
        CodecRegistry pojoCodecRegistry = CodecRegistries.
                fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                        CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        // 选择数据库 并且 注册解码器
        MongoDatabase mongoDatabase = MongoManager.getDatabase().withCodecRegistry(pojoCodecRegistry);

        MongoCollection<Person> personCollection = mongoDatabase
                .getCollection("person", Person.class);
        DeleteResult deleteResult =
                personCollection.deleteMany(Filters.eq("name", "张三"));
        System.out.println(deleteResult);
    }

}
