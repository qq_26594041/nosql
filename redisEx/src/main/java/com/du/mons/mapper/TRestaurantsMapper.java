package com.du.mons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.du.mons.bean.TRestaurants;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/** 
  *
  * @date 2023/11/12 22:19
  * @author  杜瑞龙
 */

@Mapper
public interface TRestaurantsMapper extends BaseMapper<TRestaurants> {
    int updateBatch(List<TRestaurants> list);

    int updateBatchSelective(List<TRestaurants> list);

    int batchInsert(@Param("list") List<TRestaurants> list);

    int insertOrUpdate(TRestaurants record);

    int insertOrUpdateSelective(TRestaurants record);
}