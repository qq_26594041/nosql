package com.du.mons.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/** 
  *
  * @date 2023/11/12 22:19
  * @author  杜瑞龙
 */

@Data
@TableName(value = "redis.t_restaurants")
public class TRestaurants {
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * the En Name of the restaurant
     */
    @TableField(value = "`name`")
    private String name;

    @TableField(value = "cnname")
    private String cnname;

    @TableField(value = "x")
    private Double x;

    @TableField(value = "y")
    private Double y;

    /**
     * En location of the restaurant
     */
    @TableField(value = "`location`")
    private String location;

    @TableField(value = "cnlocation")
    private String cnlocation;

    /**
     * city.district.neighbourhood
Example: Shanghai.Xuhui.Xujiahui
     */
    @TableField(value = "area")
    private String area;

    /**
     * Phone of the restaurant
     */
    @TableField(value = "telephone")
    private String telephone;

    @TableField(value = "email")
    private String email;

    @TableField(value = "website")
    private String website;

    @TableField(value = "cuisine")
    private String cuisine;

    @TableField(value = "average_price")
    private String averagePrice;

    /**
     * Indtroduction of the restaurant
     */
    @TableField(value = "introduction")
    private String introduction;

    /**
     * pics at the list, value would be:
basepath/original/picname
     */
    @TableField(value = "thumbnail")
    private String thumbnail;

    /**
     * the percentage of people like it
     */
    @TableField(value = "like_votes")
    private Integer likeVotes;

    /**
     * How many people votes
     */
    @TableField(value = "dislike_votes")
    private Integer dislikeVotes;

    /**
     * 城市id
     */
    @TableField(value = "city_id")
    private Integer cityId;

    /**
     * 1=Valid 0=Invalid
     */
    @TableField(value = "is_valid")
    private Integer isValid;

    @TableField(value = "create_date")
    private Date createDate;

    @TableField(value = "update_date")
    private Date updateDate;
}