package com.du.mons.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.checkerframework.checker.units.qual.A;

import java.io.Serializable;

/**
 * @author 杜瑞龙
 * @date 2023/5/21 16:58
 */
@Data
@Builder
@AllArgsConstructor
public class UserBean implements Serializable {

    String name;
    String password;
    Integer age;

}
