package com.du.mons.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 杜瑞龙
 * @date 2021/3/28 21:37
 */
@Data
public class RedisDemoBean implements Serializable {

    String aa;
    String d;
    String dg;
    String trh;
    String ty;
}
