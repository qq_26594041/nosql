package com.du.mons;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author du
 * @date 2020/12/2 21:25
 * 原理：cacheManager====Cache \
 *
 */
@SpringBootApplication
@MapperScan("com.du.mons.mapper")
public class RedisSpringBoot {
    public static void main(String[] args) {
        SpringApplication.run(RedisSpringBoot.class);
    }
}
