package com.du.mons.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 *   启用 activateDefaultTyping 方法把类型信息作为属性写入序列化后的数据中
 *   <code>
 *  ObjectMapper objectMapper = new ObjectMapper();
 *  objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
 *  objectMapper.activateDefaultTyping(objectMapper.getPolymorphicTypeValidator(),ObjectMapper.DefaultTyping.JAVA_LANG_OBJECT);
 * Jackson2JsonRedisSerializer<Object> objectJackson2JsonRedisSerializer =
 *  *new Jackson2JsonRedisSerializer<>(objectMapper,Object .class);
 *   </code>
 *   直接 使用  RedisSerializer.json() 更简单
 */
@Configuration
public class RedisTemplateConfiguration {

    @Bean("redisTemplate")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        // 设置key和value的序列化规则
        redisTemplate.setValueSerializer(RedisSerializer.json());
        redisTemplate.setKeySerializer(RedisSerializer.string());
        redisTemplate.setHashKeySerializer(RedisSerializer.string());
        redisTemplate.setHashValueSerializer(RedisSerializer.json());
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }
}
