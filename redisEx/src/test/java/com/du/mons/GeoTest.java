package com.du.mons;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 杜瑞龙
 * @date 2022/11/5 22:09
 */
@SpringBootTest
class GeoTest {

    static String key = "city";
    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 新增天安门故宫长城经纬度
     */
    @Test
    void geoAdd() {
        Map<String, Point> map = new HashMap<>();
        map.put("天安门", new Point(116.403963, 39.915119));
        map.put("故宫", new Point(116.403414, 39.924091));
        map.put("长城", new Point(116.024067, 40.362639));
        redisTemplate.opsForGeo().add(key, map);

    }


    /**
     * 获取地理位置的坐标
     */
    @Test
    void position() {

        List<Point> list = redisTemplate.opsForGeo().position(key, "故宫", "长城");

        System.out.println(list.toString());
    }


    /**
     * geohash算法生成的base32编码值
     */
    @Test
    void hash() {
        List<String> hash = redisTemplate.opsForGeo().hash(key, "故宫", "长城");
        System.out.println(hash.toString());
    }

    /**
     * 计算两个位置之间的距离
     */
    @Test
    void distance() {
        Distance distance = redisTemplate.opsForGeo().distance(key, "故宫", "长城",
                RedisGeoCommands.DistanceUnit.KILOMETERS);
        System.out.println(distance.toString());
    }

    /**
     * 通过经度，纬度查找附近的
     * 北京王府井位置116.418017,39.914402,这里为了方便讲课，故意写死
     */
    @Test
    void radiusByxy() {
        //这个坐标是北京王府井位置
        Circle circle = new Circle(116.418017, 39.914402, Metrics.MILES.getMultiplier());
        //返回50条
        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().includeDistance()
                .includeCoordinates().sortAscending().limit(10);
        GeoResults<RedisGeoCommands.GeoLocation<Object>> radius = redisTemplate.opsForGeo().radius(key, circle, args);
        System.out.println(radius.toString());

    }

    /**
     * 通过地方查找附近
     */

    @Test
    void radiusByMember() {
        String member = "天安门";
        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().includeDistance()
                .includeCoordinates().sortAscending().limit(10);
        //半径10公里内
        Distance distance = new Distance(10, Metrics.KILOMETERS);
        GeoResults<RedisGeoCommands.GeoLocation<Object>> radius = redisTemplate.opsForGeo().radius(key, member, distance, args);

        System.out.println(radius.toString());
    }


}
