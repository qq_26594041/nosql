package com.du.mons;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;
import java.util.Set;

/**
 * @author 杜瑞龙
 * @date 2022/11/8 21:41
 */
@SpringBootTest
@Slf4j
class setDemo {
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    /**
     * 交集，共同关注的人
     */
    @Test
    void testIntersect() {
        SetOperations<String, String> stringStringSetOperations = stringRedisTemplate.opsForSet();
        stringStringSetOperations.add("followingA", "1", "2", "3", "4", "5");
        stringStringSetOperations.add("followingB", "3", "4", "7", "8", "9");
        Set<String> intersect = stringStringSetOperations.intersect(List.of("followingA", "followingB"));
    }

    /**
     * 差集 推送可能认识的人
     */
    @Test
    void tesDifference() {
        SetOperations<String, String> stringStringSetOperations = stringRedisTemplate.opsForSet();
        stringStringSetOperations.add("followingA", "1", "2", "3", "4", "5");
        stringStringSetOperations.add("followingB", "3", "4", "7", "8", "9");
        Set<String> intersect = stringStringSetOperations.difference(List.of("followingB", "followingA"));
    }

    /**
     * 抽奖
     */
    @Test
    void tesDraw() {
        SetOperations<String, Object> setOperations = redisTemplate.opsForSet();
        for (int i = 0; i < 1000; i++) {
            setOperations.add("draw_weixin", i);
        }
        log.info("共有{}参与抽奖", setOperations.size("draw_weixin"));

        log.info("恭喜用户 {} 中一等奖 可继续抽奖", setOperations.randomMembers("draw_weixin", 10));
        log.info("恭喜用户 {} 中二等奖 不可继续抽奖", setOperations.pop("draw_weixin", 10));

    }

    /**
     * 点赞
     */
    @Test
    void tesLike() {
        SetOperations<String, Object> setOperations = redisTemplate.opsForSet();
        for (int i = 0; i < 10; i++) {
            setOperations.add("like", i);
        }

        log.info("取消点赞{}参与", setOperations.remove("draw_weixin",1));

        log.info("所有点赞用户 {} ", setOperations.members("draw_weixin"));

        log.info("用户[{}]点赞", setOperations.isMember("draw_weixin", 1));

    }
}
