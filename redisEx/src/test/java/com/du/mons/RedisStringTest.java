package com.du.mons;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;


@SpringBootTest
class RedisStringTest {

    @Autowired
    StringRedisTemplate stringRedisTemplate;


   static String LOCK_KEY = "LOCK_KEY";

    /**
     * 分布式 锁  无原子性
     */
    @Test
    void testLock1(){
        stringRedisTemplate.opsForValue().setIfAbsent("key","value");
        stringRedisTemplate.expire("key",10, TimeUnit.SECONDS);
    }

    /**
     * String 数据类型方法测试
     */
    @Test
    void stringRedisadd() {

        /*
            过时key
         */

        stringRedisTemplate.opsForValue().set("xin", String.valueOf(1236));

        /*
         * 获取
         */
        String setTest = stringRedisTemplate.opsForValue().get("setTest");
        //追加 key 不存在时创建
        stringRedisTemplate.opsForValue().append("SDemo", "hello String");

    }
}
