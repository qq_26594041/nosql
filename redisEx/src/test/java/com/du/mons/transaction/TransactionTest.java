package com.du.mons.transaction;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 杜瑞龙
 * @date 2023/5/21 21:33
 */
@SpringBootTest
public class TransactionTest {

    @Autowired
    RedisTemplate redisTemplate;

    @Transactional
    @Test
    public void  get(){
        redisTemplate.opsForValue().set("transaction","trffue2");

        System.out.println("");

    }
}
