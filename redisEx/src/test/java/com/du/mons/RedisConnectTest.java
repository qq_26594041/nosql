package com.du.mons;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author 杜瑞龙
 * @date 2022/9/25 16:07
 */
@SpringBootTest
public class RedisConnectTest {

    @Autowired
    RedisTemplate<String,Object>  redisTemplate;

    @Test
    public void  connectTest(){

        String ping = redisTemplate.getConnectionFactory().getConnection().ping();
        System.out.println(ping);
    }
}
