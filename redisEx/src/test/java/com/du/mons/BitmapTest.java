package com.du.mons;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.util.CollectionUtils;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


import static cn.hutool.core.date.DatePattern.SIMPLE_MONTH_PATTERN;

/**
 * @author 杜瑞龙
 * &#064;date  2022/11/5 22:09
 */
@SpringBootTest
class BitmapTest {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    /**
     * 统计签到次数
     * @param userId        用户 id
     * @param localDateTime 日期
     * @return 签到次数
     */
    Long getSignDayCount(Integer userId, LocalDateTime localDateTime) {
        String signKey = buildSignKey(userId, localDateTime);
        return redisTemplate.execute((RedisCallback<Long>) con ->
                con.stringCommands().bitCount(signKey.getBytes()));
    }

    /**
     * 统计连续签到的次数
     *
     * @param userId        用户 id
     * @param localDateTime 日期
     * @return 连续签到次数
     */
    int getContinuousSignDayCount(Integer userId, LocalDateTime localDateTime) {
        ValueOperations<String, Object> bitOp = redisTemplate.opsForValue();
        int dayOfMonth = localDateTime.getDayOfMonth();
        String signKey = buildSignKey(userId, localDateTime);
        BitFieldSubCommands bitFieldSubCommands = BitFieldSubCommands.create()
                .get(BitFieldSubCommands.BitFieldType.unsigned(dayOfMonth)).valueAt(0);
        //获取本月截止今天为止的所有的签到记录
        List<Long> result = bitOp.bitField(signKey, bitFieldSubCommands);

        // 没有任何签到结果
        if (CollectionUtils.isEmpty(result)) {
            return 0;
        }

        //num为0，直接返回0
        Long num = result.get(0);
        if (num == null || num == 0) {
            return 0;
        }

        /*
         * 如果为0，说明未签到，结束
         * 如果不为0，说明已签到，计数器+1
         * 让这个数字与1做与运算，得到数字的最后一个bit位
         *  判断这个bit位是否为0
         *  把数字右移一位，抛弃最后一个bit位，继续下一个bit位
         */
        int count = 0;
        while ((num & 1) != 0) {
            count++;
            num >>>= 1;
        }
        return count;
    }

    /**
     * 获取当月签到情况
     *
     * @param userId        用户 id
     * @param localDateTime 日期
     */
    public Map<String, Boolean> getSignInfo(Integer userId,  LocalDateTime localDateTime) {


        String signKey = buildSignKey(userId, localDateTime);
        // 构建一个自动排序的 Map
        Map<String, Boolean> signInfo = new TreeMap<>();
        // 获取某月的总天数（考虑闰年）

        int dayOfMonth = localDateTime.toLocalDate().lengthOfMonth();

        // bitfield user:sign:5:202011 u30 0
        BitFieldSubCommands bitFieldSubCommands = BitFieldSubCommands.create()
                .get(BitFieldSubCommands.BitFieldType.unsigned(dayOfMonth))
                .valueAt(0);
        List<Long> list = redisTemplate.opsForValue().bitField(signKey, bitFieldSubCommands);
        if (list == null || list.isEmpty()) {
            return signInfo;
        }
        long v = list.get(0) == null ? 0 : list.get(0);
        // 从低位到高位进行遍历，为 0 表示未签到，为 1 表示已签到
        for (int i = dayOfMonth; i > 0; i--) {
            /*
                签到：  yyyy-MM-01 true
                未签到：yyyy-MM-01 false
             */
            boolean flag = v >> 1 << 1 != v;
            signInfo.put(DateUtil.format(localDateTime, "yyyy-MM-dd"), flag);
            v >>= 1;
        }
        return signInfo;
    }

    /**
     * 返回签到 key
     *
     * @param userId        用户 id
     * @param localDateTime 日期
     * @return 签到 key
     */
    String buildSignKey(Integer userId, LocalDateTime localDateTime) {
        return String.format("user:sign:%d:%s", userId, DateUtil.format(localDateTime, SIMPLE_MONTH_PATTERN));
    }

    LocalDateTime getLocalDate(String dateString) {
        return DateUtil.parseLocalDateTime(dateString, SIMPLE_MONTH_PATTERN);
    }
}
