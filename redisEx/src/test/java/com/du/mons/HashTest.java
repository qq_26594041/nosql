package com.du.mons;


import com.du.mons.bean.RedisDemoBean;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Map;

/**
 * @author 杜瑞龙
 * @date 2022/11/5 22:09
 */
@SpringBootTest
class HashTest {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate<String,Object> redisTemplate;
    @Test
    void  hashTest(){
        HashOperations<String, Object, Object> stringObjectObjectHashOperations = stringRedisTemplate.opsForHash();
        RedisDemoBean redisDemoBean = new RedisDemoBean();
        redisDemoBean.setAa("ss");
        redisDemoBean.setTrh("dd");
        stringRedisTemplate.opsForHash().putIfAbsent("h5","h51","h52");
        redisTemplate.opsForHash().putIfAbsent("ho","h51",redisDemoBean);
        Map<String, String> stringStringMap = Map.of("1", "1", "4", "3", "3", "9");
        stringRedisTemplate.opsForHash().putAll("all",stringStringMap);

        Map<Object, Object> all = stringRedisTemplate.opsForHash().entries("all");

    }

    /**
     * 购物车
     */
    @Test
    void  hashTest1(){
        HashOperations<String, Object, Object> stringObjectObjectHashOperations = redisTemplate.opsForHash();
        stringObjectObjectHashOperations.put("car","扇子",1);
        stringObjectObjectHashOperations.put("car","护甲",1);
        stringObjectObjectHashOperations.increment("car","护甲",3);
        Long car = stringObjectObjectHashOperations.size("car");
        Map<Object, Object> cars = stringObjectObjectHashOperations.entries("car");
        System.out.println("");

    }
}
