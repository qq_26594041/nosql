package com.du.mons;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;

import java.time.Instant;
import java.util.Objects;
import java.util.Set;

/**
 * @author 杜瑞龙
 * @date 2022/11/8 21:41
 */
@SpringBootTest
@Slf4j
public class ZsetDemo {
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate<String,Object> redisTemplate;

    /**
     * 关注排名
     */
    @Test
     void test() {
        SetOperations<String, String> setOperations = stringRedisTemplate.opsForSet();
        ZSetOperations<String, String> zSetOperations = stringRedisTemplate.opsForZSet();
        for (String followingA : Objects.requireNonNull(setOperations.members("followingA"))) {
            zSetOperations.add("feed",followingA, Instant.now().toEpochMilli());
        }
    }

    /**
     * 查询排序
     */
    @Test
     void testSort() {
        SetOperations<String, String> setOperations = stringRedisTemplate.opsForSet();
        ZSetOperations<String, String> zSetOperations = stringRedisTemplate.opsForZSet();
        //闭区间, 包含两端元素
        Set<String> feed = zSetOperations.reverseRange("feed", 0, 9);
        log.info(" 逆序 ：{}",feed);
    }

    /**
     * 查询
     */
    @Test
    void testFind() {
        ZSetOperations<String, String> zSetOperations = stringRedisTemplate.opsForZSet();

        // 获取排名
        Long feed = zSetOperations.reverseRank("feed", "3");

        //获取分数
        Double feed1 = zSetOperations.score("feed", "3");

        System.out.println();


    }
}
