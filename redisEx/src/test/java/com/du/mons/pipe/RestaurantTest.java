package com.du.mons.pipe;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.du.mons.bean.TRestaurants;
import com.du.mons.mapper.TRestaurantsMapper;
import com.google.common.collect.Maps;
import jakarta.annotation.Resource;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.List;
import java.util.Map;

@SpringBootTest
@Slf4j
public class RestaurantTest {

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    TRestaurantsMapper tRestaurantsMapper;

    // 逐行插入
    @Test
    void testSyncForHash() {
        List<TRestaurants> restaurants = tRestaurantsMapper.selectList(null);

        TimeInterval timer = DateUtil.timer();
        restaurants.forEach(restaurant -> {
            Map<String, Object> restaurantMap = BeanUtil.beanToMap(restaurant);
            String key = "res" + restaurant.getId();
            redisTemplate.opsForHash().putAll(key, restaurantMap);
        });

        log.info("执行时间：{}", timer.intervalSecond()); // 执行时间：118957
    }

    /**
     * Pipeline 管道插入
     */
    @Test
    void testSyncForHashPipeline() {
        List<TRestaurants> restaurants = tRestaurantsMapper.selectList(null);

        TimeInterval timer = DateUtil.timer();
        List<Long> list = redisTemplate.executePipelined((RedisCallback<Long>) connection -> {
            for (TRestaurants restaurant : restaurants) {
                try {
                    String key = "res" + restaurant.getId();
                    Map<String, Object> restaurantMap = BeanUtil.beanToMap(restaurant);
                    StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
                    Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
                    Map<byte[], byte[]> restaurantStringMap = Maps.newHashMap();
                    restaurantMap.forEach((k, v) -> {
                        restaurantStringMap.put(stringRedisSerializer.serialize(k), jackson2JsonRedisSerializer.serialize(v));
                    });
                    connection.hashCommands().hMSet(stringRedisSerializer.serialize(key), restaurantStringMap);
                } catch (Exception e) {
                    log.info(restaurant.toString());
                    continue;
                }
            }
            return null;
        });

        log.info("执行时间：{}", timer.intervalSecond());
    }

}