package com.du.mons;


import com.du.mons.bean.UserBean;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 杜瑞龙
 * @date 2022/11/8 21:41
 */
@SpringBootTest
class ListDemo {
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 分页查询
     */
    @Test
    void test(){
        ArrayList<UserBean> userBeans = new ArrayList<>();
        for (int i = 0; i <20 ; i++) {
            UserBean build = UserBean.builder().age(11+i).password("oo").name("ddd").build();
            redisTemplate.opsForList().rightPush("users",build);

        }
       redisTemplate.opsForList().leftPushAll("users",userBeans);

        int page=1;
        int size=40;
        int start =(page-1)*size;
        int end =start+size-1;
        try {
            List users = redisTemplate.opsForList().range("users", 0, -1);

            if (users != null) {
                users.forEach(e->{


                    System.out.println(e.toString());
                });
            }else{
                System.out.println("数据查询");
            }
        }catch (Exception e){
            //一般都是 redis 超时
            //数据查询
        }
    }
}
