package com.du.mons;


import com.du.mons.bean.UserBean;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author 杜瑞龙
 * @date 2022/11/5 21:35
 */
@SpringBootTest
public class JsonTest {

    @Autowired
    RedisTemplate<String,Object> redisTemplate;

    @Test
    void redisTempTestMy(){
        UserBean build = UserBean.builder().age(11).password("oo").name("ddd").build();
        redisTemplate.opsForValue().set("user007", build);

        System.out.println();
    }
}
