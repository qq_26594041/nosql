package com.du.mons;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author 杜瑞龙
 * @date 2022/11/5 22:09
 */
@SpringBootTest
@Slf4j
public class HyperloglogTest {

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @Test
    void hashTest() {

        log.info("------模拟后台有用户点击，每个用户ip不同");
        String ip = null;
        for (int i = 1; i <= 2000; i++) {
            Random random = new Random();
            ip = random.nextInt(255) + "." + random.nextInt(255) + "." + random.nextInt(255) + "." + random.nextInt(255);
            Long hll = redisTemplate.opsForHyperLogLog().add("hll", ip);
            log.info("ip={},该ip访问过的次数={}", ip, hll);
        }
    }

    @Test
    void count(){
        Long hll = redisTemplate.opsForHyperLogLog().size("hll");
        System.out.println(hll);
    }}
