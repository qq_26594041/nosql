package com.du.mons;


import com.du.mons.bean.UserBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;


/**
 * @author 杜瑞龙
 * @date 2024/11/14 23:40
 */
@SpringBootTest
public class SerialDemo {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 通过 RedisTemplate 读取 Key 为 stringRedisTemplate 的 Value，
     * 使用 StringRedisTemplate 读取 Key 为 redisTemplate 的 Value：
     * 结果是，两次都无法读取到 Value：
     */
    @PostConstruct
    public void init() throws JsonProcessingException {
        redisTemplate.opsForValue().set("redisTemplate", new UserBean("zhuye", "dd", 23));
        stringRedisTemplate.opsForValue().set("stringRedisTemplate",
                objectMapper.writeValueAsString(new UserBean("zhuye", "dd", 23)));
        Object stringRedisTemplate1 = redisTemplate.opsForValue().get("stringRedisTemplate");
        String redisTemplate1 = stringRedisTemplate.opsForValue().get("redisTemplate");
    }
}
